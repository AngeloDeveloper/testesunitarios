import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import { Observable } from 'rxjs';

export interface retorno{
  elements: nome[]
}

interface nome{
  name: string
  id: number
}

export const url =
    "https://mocki.io/v1/1c0c9503-ec55-46e1-947c-6fd8f4c136be";


@Injectable({
  providedIn: 'root'
})
export class NameServiceService {

  constructor(private http: HttpClient) { }

  listar(): Observable<retorno> {
    return this.http.get<retorno>(url);

  }
}