import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NameServiceService, retorno } from './name-service.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { ComServiceComponent } from '../com-service/com-service.component';
import { By } from '@angular/platform-browser';
import { Component, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppModule } from '../app.module';

let mockNomesServiceData = null as any;

class MockNomesService {
  listar(): Observable<any> {
    return mockNomesServiceData;
  }
}

describe('NameServiceService', () => {

  let service: NameServiceService;
   beforeEach(async() => {
    TestBed.configureTestingModule({
      declarations:[ComServiceComponent],
      providers: [{ provide: NameServiceService, useClass: MockNomesService }],
      imports: [HttpClientTestingModule],
      schemas:[CUSTOM_ELEMENTS_SCHEMA]
    })
    service = TestBed.inject(NameServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a list of names', () => {

    const dadosMockados =
    { "elements": [{ "name": "nome 1", "id": 1 }, { "name": "nome 2", "id": 2 }] }
    ;

    mockNomesServiceData = of(dadosMockados);

    service.listar().subscribe(response => {
      expect(response.elements.length).toEqual(2);
      expect(response.elements[0].name).toEqual('nome 1');
      expect(response.elements[1].name).toEqual('nome 2');
      expect(response.elements[1].id).toEqual(2)
    });

  });


  it('deve renderizar a lista de nomes', () => {
    const fixture = TestBed.createComponent(ComServiceComponent);

    const dadosMockados =
      { "elements": [{ "name": "nome 1", "id": "1" }, { "name": "nome 2", "id": "2" }] }
      ;

    mockNomesServiceData = of(dadosMockados);

    fixture.detectChanges();
    const celulasDaTabela = fixture.debugElement
      .queryAll(By.css('td'));


    alert(celulasDaTabela)
    expect(celulasDaTabela[0].nativeElement.innerHTML)
      .toBe(dadosMockados.elements[0].id);

    expect(celulasDaTabela[1].nativeElement.innerHTML)
      .toBe(dadosMockados.elements[0].name);
   
  });


});
