import { Component, OnInit } from '@angular/core';
import { NameServiceService } from '../service/name-service.service';

@Component({
  selector: 'app-com-service',
  templateUrl: './com-service.component.html',
  styleUrls: ['./com-service.component.css']
})
export class ComServiceComponent implements OnInit {

  nomes: any = [];
  ocorreuErro: boolean = false;

  constructor(private nomeService: NameServiceService) { }

  ngOnInit() {
    this.nomeService
      .listar()
      .subscribe(
        resposta => {
          this.ocorreuErro = false;
          this.nomes = resposta.elements;
        },
        error => {
          console.error('Erro ao obter os registros', error);
          this.ocorreuErro = true;
        }
      );
  }

  retornaQuantidadeDeNomes(){
    return this.nomes.length
  }

}
