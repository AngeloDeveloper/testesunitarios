import { HttpClientModule } from '@angular/common/http';
import { HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { NameServiceService } from '../service/name-service.service';
import { ComServiceComponent } from './com-service.component';



let mockNomesServiceData = null as any;

class MockNomesService {
  listar(): Observable<any> {
    return mockNomesServiceData;
  }
}


describe('ComServiceComponent', () => {
  let component: ComServiceComponent;
  let fixture: ComponentFixture<ComServiceComponent>;
 

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:[HttpClientModule],
      providers:[{ provide: NameServiceService, useClass: MockNomesService }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    const dadosMockados =
    { "elements": [{ "name": "nome 1", "id": 1 }, { "name": "nome 2", "id": 2 }] }
    ;

    mockNomesServiceData = of(dadosMockados);

    fixture = TestBed.createComponent(ComServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("deve contar os nomes",()=>{

      fixture.detectChanges();
      expect(fixture.componentInstance.retornaQuantidadeDeNomes()).toBe(2)
  });


});