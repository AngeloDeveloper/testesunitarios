import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NoServiceComponent } from './no-service/no-service.component';
import { ComServiceComponent } from './com-service/com-service.component';


@NgModule({
  declarations: [
    AppComponent,
    NoServiceComponent,
    ComServiceComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
