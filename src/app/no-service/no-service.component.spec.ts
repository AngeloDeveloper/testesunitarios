import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { NoServiceComponent } from './no-service.component';


describe('NoServiceComponent', () => {
  let component: NoServiceComponent;
  let fixture: ComponentFixture<NoServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NoServiceComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should list names', () => {
    component.nomes = [{ name: 'elemento 1', id: 1 }, { name: ' elemento 2', id: 2 }]
    fixture.detectChanges();


    const celulasDaTabela = fixture.debugElement
      .queryAll(By.css('td'));


    expect(celulasDaTabela[0].nativeElement.innerHTML)
      .toBe("1");

    expect(celulasDaTabela[1].nativeElement.innerHTML)
      .toBe("elemento 1");
  });


});
